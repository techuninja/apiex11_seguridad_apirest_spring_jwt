package com.udemy.apiex11_seguridad_apirest_spring_jwt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Apiex11SeguridadApirestSpringJwtApplication {

    public static void main(String[] args) {
        SpringApplication.run(Apiex11SeguridadApirestSpringJwtApplication.class, args);
    }

}
