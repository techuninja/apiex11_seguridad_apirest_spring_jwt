package com.udemy.apiex11_seguridad_apirest_spring_jwt.controller;

import com.kastkode.springsandwich.filter.annotation.Before;
import com.kastkode.springsandwich.filter.annotation.BeforeElement;
import com.udemy.apiex11_seguridad_apirest_spring_jwt.components.AuthHandler;
import com.udemy.apiex11_seguridad_apirest_spring_jwt.components.JWTBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/apitechu/jwt")
public class HelloWorldController {

    @Autowired
    JWTBuilder jwtBuilder;

    @GetMapping("/tokenget")
    public String tokenget(@RequestParam(value="nombre", defaultValue="Tech U!") String name){
        return jwtBuilder.generateToken(name, "admin");
    }

    @GetMapping(path="/hello", headers = {"Authorization"})
    @Before(@BeforeElement(AuthHandler.class))
    public String helloWorld(){
        String s = "Hello from JWT demo...";
        return s;
    }
}
